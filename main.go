package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/mitchellh/go-homedir"
	"github.com/prometheus/common/version"
	"gopkg.in/yaml.v3"
)

type Config struct {
	CurrentContext string     `yaml:"current-context"`
	Contexts       []*Context `yaml:"contexts"`
}

type Context struct {
	Name string `yaml:"name"`
}

var (
	answers = struct {
		Context string
	}{}

	versionFlag = flag.Bool("version", false, "Prints version and exit")
)

func loadConfig() (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}
	kubeConfigPath := path.Join(home, ".kube", "config")
	b, err := ioutil.ReadFile(kubeConfigPath)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = yaml.Unmarshal(b, config)
	if err != nil {
		return nil, err
	}
	if len(config.Contexts) == 0 {
		return nil, errors.New("Empty context list")
	}
	return config, nil
}

func createQuestion(config *Config) *survey.Question {
	options := []string{}
	for _, context := range config.Contexts {
		options = append(options, context.Name)
	}
	question := &survey.Question{
		Name:     "context",
		Validate: survey.Required,
		Prompt: &survey.Select{
			PageSize: 50,
			Message:  "Choose a Kubernetes context:",
			Options:  options,
			Default:  config.CurrentContext,
		},
	}
	return question
}

func setContext(context string) error {
	kubectlPath, err := exec.LookPath("kubectl")
	if err != nil {
		return err
	}
	cmd := []string{
		"config",
		"use-context",
		context,
	}
	out, err := exec.Command(kubectlPath, cmd...).CombinedOutput()
	result := strings.TrimSpace(string(out))
	fmt.Println(result)
	if err != nil {
		return err
	}
	return nil
}

func getUserSuggestionContext(config *Config, args []string) (context string) {
	var contexts []string
	if len(args) == 0 {
		return
	}
	for _, ctx := range config.Contexts {
		if strings.Contains(ctx.Name, args[0]) {
			contexts = append(contexts, ctx.Name)
		}
	}
	if len(contexts) == 1 {
		context = contexts[0]
	}
	return
}

func run() int {
	if *versionFlag {
		fmt.Println(version.Print("kctx"))
		return 0
	}
	config, err := loadConfig()
	if err != nil {
		fmt.Println(err)
		return 1
	}
	var selectedContext string
	userSuggestionContext := getUserSuggestionContext(config, os.Args[1:])
	if len(userSuggestionContext) > 0 {
		selectedContext = userSuggestionContext
	} else {
		question := createQuestion(config)
		err = survey.Ask([]*survey.Question{question}, &answers)
		if err != nil {
			fmt.Println(err)
			return 1
		}
		selectedContext = answers.Context
	}
	err = setContext(selectedContext)
	if err != nil {
		fmt.Println(err)
		return 1
	}
	return 0
}

func main() {
	flag.Parse()
	os.Exit(run())
}
