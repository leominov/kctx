module github.com/leominov/kctx

go 1.14

require (
	github.com/AlecAivazis/survey/v2 v2.0.7
	github.com/mitchellh/go-homedir v1.1.0
	github.com/prometheus/common v0.10.0
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c
)
